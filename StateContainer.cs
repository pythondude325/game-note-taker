using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text.Json;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using gnt.Pages;

namespace gnt
{
    public class StateContainer
    {
        private readonly ILocalStorageService _localStorage;

        private bool _pulledNotes = false;
        public List<TodoItem> Notes = new List<TodoItem>();
        public event Action OnChangeNotes;

        public async void PullNotes()
        {
            if (_pulledNotes == false)
            {
                var storedContent = await _localStorage.GetItemAsStringAsync("todo_items");
                if (string.IsNullOrEmpty(storedContent))
                {
                    Notes = new List<TodoItem>();
                }
                else
                {
                    Notes = JsonSerializer.Deserialize<List<TodoItem>>(storedContent);
                }

                _pulledNotes = true;
                OnChangeNotes?.Invoke();
            }
        }

        public async Task PushNotes()
        {
            await _localStorage.SetItemAsync("todo_items", JsonSerializer.Serialize(Notes));
        }

        public StateContainer(ILocalStorageService localStorageService)
        {
            _localStorage = localStorageService;
        }
    }
}