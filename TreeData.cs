using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Microsoft.VisualBasic;

namespace gnt
{
    public enum TodoItemType
    {
        Task,
        Note,
    }

    public class TodoItem
    {
        public TodoItemType ItemType { get; set; }
        public bool Collapsed { get; set; }
        public bool Completed { get; set; }
        public string Content { get; set; }
        public List<TodoItem> Children { get; set; }

        public TodoItem(TodoItemType itemType, string content)
        {
            ItemType = itemType;
            Content = content;
            Children = new List<TodoItem>();
        }

        public void AddNote(TodoItem note)
        {
            Children.Add(note);
        }

        public void RemoveChild(TodoItem note)
        {
            Children.Remove(note);
        }

        [JsonIgnore]
        public int TasksTotal => ItemType switch
        {
            TodoItemType.Note => 0,
            TodoItemType.Task => 1 + Children.Sum(child => child.TasksTotal),
            _ => throw new ArgumentOutOfRangeException()
        };

        [JsonIgnore]
        public int TasksComplete => ItemType switch
        {
            TodoItemType.Note => 0,
            TodoItemType.Task => (Completed ? 1 : 0) + Children.Sum(child => child.TasksComplete),
            _ => throw new ArgumentOutOfRangeException()
        };
    }
}